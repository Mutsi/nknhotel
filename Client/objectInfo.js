var ObjectInfo = {};

ObjectInfo["24"] = {
  name: "Rubber Duck"
}
ObjectInfo["8"] = {
  name: "NKN Poster"
}
ObjectInfo["16"] = {
  name: "NKN Poster"
}
ObjectInfo["21"] = {
  name: "Stool"
}
ObjectInfo["32"] = {
  name: "NKN Box"
}

var RarityValues = Object.freeze([
  "Common",
  "Uncommon",
  "Rare",
  "Epic",
  "Legendary"
]);

function RarityDistribute(value) {
  return Math.pow(value, 3);
}

function GetRarity(hash) {
  //Max 0xFFFF = 65535
  //5 Rarity values
  var rarityValue = parseInt('0x' + hash.substring(0, 4));
  var rarityScalar = rarityValue / 65535.0;
  var rarityValue = RarityDistribute(rarityScalar);
  var rarity = Math.floor(rarityValue * 5);

  return RarityValues[rarity];
}