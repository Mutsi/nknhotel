class Game {
  constructor(address, canvas, ctx) {
    this.currentRoom = '';
    this.remotePlayers = [];
    this.localPlayer = null;
    this.clientAddress = address;
    this.canvas = canvas;
    this.ctx = ctx;
    this.bubbleLife = 5.0;
    this.chatMessages = [];
    this.selectedObject = null;

    this.floorMap = [
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 1, 1, 1, 1, 1, 1, 1, 1, 1],
      [0, 1, 1, 1, 1, 1, 1, 1, 1, 1],
      [0, 1, 1, 1, 1, 1, 1, 1, 1, 1],
      [0, 1, 1, 1, 1, 1, 1, 1, 1, 1],
      [0, 1, 1, 1, 1, 1, 1, 1, 1, 1],
      [0, 1, 1, 1, 1, 1, 1, 1, 1, 1],
      [0, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    ];
    this.objectMap = [];
    this.getRoom(address);

    this.tileGraphics = [];
    this.graphicsLoaded = false;
    this.gridMouse = [0, 0];
    this.tileMouse = [0, 0];
    this.currentPath = [];

    //invert for EasyStar
    var transpose = this.floorMap[0].map((col, i) => this.floorMap.map(row => row[i]));

    easystar.setGrid(transpose);
    easystar.setAcceptableTiles([1]);
    easystar.enableDiagonals();
    easystar.disableCornerCutting();

    //camera offset
    this.camX = 320; //0 + this.canvas.offsetLeft / 2;
    this.camY = 54;


    // Images to be loaded and used.
    // Tutorial Note: As water is loaded first it will be represented by a 0 on the map and land will be a 1.
    var tileGraphicsToLoad = ["./assets/set/64set.png", "./assets/set/chicken.png", "./assets/isoCursor.png", "./assets/set/habbo.png"];
    var tileGraphicsLoaded = 0;
    for (var i = 0; i < tileGraphicsToLoad.length; i++) {
      this.tileGraphics[i] = new Image();
      this.tileGraphics[i].src = tileGraphicsToLoad[i];
      this.tileGraphics[i].onload = function() {
        // Once the image is loaded increment the loaded graphics count and check if all images are ready.
        tileGraphicsLoaded++;
        if (tileGraphicsLoaded === tileGraphicsToLoad.length) {
          this.graphicsLoaded = true;
        };
      }.bind(this);
    }
  }

  drawTileFromSet(id, posX, posY) {
    var x = Math.floor(id / 8);
    var y = id % 8;

    this.ctx.drawImage(this.tileGraphics[0], x * 64, y * 64, 64, 64, posX, posY, 64, 64);
  }

  updatePlayer(address, name, x, y, rotation) {
    for (var i = 0; i < this.remotePlayers.length; i++) {
      if (this.remotePlayers[i].address == address) {
        this.remotePlayers[i].nx = x;
        this.remotePlayers[i].ny = y;
        this.remotePlayers[i].name = name;
        this.remotePlayers[i].rotation = rotation;
        this.remotePlayers[i].lastUpdate = new Date().getTime();
        return;
      }
    }
    //Doesnt exist add
    var player = {
      "address": address,
      "x": x,
      "y": y,
      "nx": x,
      "ny": y,
      "name": name,
      "rotation": rotation,
      "lastUpdate": new Date().getTime()
    }
    this.remotePlayers.push(player);
  }

  keyDownHandler(e) {
    e = e || window.event;
    if (e.keyCode == '38') {
      // up arrow
      this.camY--;
    } else if (e.keyCode == '40') {
      // down arrow
      this.camY++;
    } else if (e.keyCode == '37') {
      // left arrow
      this.camX--;
    } else if (e.keyCode == '39') {
      // right arrow
      this.camX++;
    }
  }

  keyUpHandler(e) {

  }

  drawBall(x, y, ballRadius, color) {
    this.ctx.beginPath();
    this.ctx.arc(x, y, ballRadius, 0, Math.PI * 2);
    this.ctx.fillStyle = color;
    this.ctx.fill();
    this.ctx.closePath();
  }

  drawSpeechBubbles() {
    for (var i = 0; i < this.chatMessages.length; i++) {
      if (this.chatMessages[i] == null)
        continue;
      //var lifeTime = (new Date().getTime() - this.localPlayer.chatMessages[i].time) / 1000;
      this.speechBubble(this.chatMessages[i].text, this.chatMessages[i].offset, 128 - i * 32, 1.0);
    }
  }

  lerp(start, end, amt) {
    return (1 - amt) * start + amt * end
  }

  speak(message) {
    this.addMessage(this.localPlayer.address, message)
    sendChatMessage(message);
  }

  addMessage(address, message) {
    this.lastMessageTime = new Date().getTime();
    if (address == '' && message == '') {
      this.chatMessages.unshift(null);
    } else {
      var offset = 0;
      var client = this.getPlayerByKey(address);
      message = client.name + " : " + message;
      var textWidth = Math.floor(this.ctx.measureText(message).width);

      offset = client.x - textWidth / 2.0;
      var overshootWidth = this.canvas.width - (offset + textWidth);
      if (overshootWidth < 0)
        offset += overshootWidth - 32;
      if (offset < 0)
        offset = 32;

      this.chatMessages.unshift({
        sender: address,
        text: message,
        offset: offset,
        time: new Date().getTime()
      });
    }
  }

  drawMap() {
    // Set as your tile pixel sizes, alter if you are using larger tiles.
    var tileH = 32;
    var tileW = 32;

    var playerIso = this.isoToCartesian(this.localPlayer.x - this.camX - 16, this.localPlayer.y - this.camY - 16);
    var playerTile = this.getTileCoordinates(playerIso[0], playerIso[1], 32);

    var mouseIso = this.isoToCartesian(this.localPlayer.x - this.camX - 16, this.localPlayer.y - this.camY - 16);
    var mouseTile = this.getTileCoordinates(playerIso[0], playerIso[1], 32);

    //PreCalculate which tile remotePlayers are on
    var remotePlayersTileMap = {};
    for (var i = 0; i < this.remotePlayers.length; i++) {
      var remoteIso = this.isoToCartesian(this.remotePlayers[i].x - this.camX - 16, this.remotePlayers[i].y - this.camY - 16);
      var remoteTile = this.getTileCoordinates(remoteIso[0], remoteIso[1], 32);
      var mapKey = remoteTile[0] + ":" + remoteTile[1];
      if (remotePlayersTileMap[mapKey] == undefined)
        remotePlayersTileMap[mapKey] = [];
      remotePlayersTileMap[mapKey] = i;
    }

    // loop through our map and draw out the image represented by the number.
    var floorTile;
    for (var i = 0; i < this.floorMap.length; i++) {
      for (var j = 0; j < this.floorMap[i].length; j++) {
        floorTile = this.floorMap[i][j];

        // Draw the represented image number, at the desired X & Y coordinates followed by the graphic width and height.
        var iso = this.cartesianToIso(i, j);
        var worldX = iso[0] * 32 + this.camX;
        var worldY = iso[1] * 32 + this.camY;
        //this.ctx.drawImage(this.tileGraphics[floorTile], worldX, worldY);
        this.drawTileFromSet(5, worldX, worldY);
      }
    }

    //Draw objects
    // loop through our map and draw out the image represented by the number.
    var objectTile;
    var objectTileId;
    for (var i = 0; i < this.objectMap.length; i++) {
      for (var j = 0; j < this.objectMap[i].length; j++) {
        objectTile = this.objectMap[i][j];
        objectTileId = objectTile.ID - 1;

        // Draw the represented image number, at the desired X & Y coordinates followed by the graphic width and height.
        var iso = this.cartesianToIso(i, j);
        var worldX = iso[0] * 32 + this.camX;
        var worldY = iso[1] * 32 + this.camY;
        //this.ctx.drawImage(this.tileGraphics[objectTileId], worldX, worldY);

        //Draw Cursor
        if (i == this.tileMouse[0] && j == this.tileMouse[1]) {
          this.ctx.drawImage(this.tileGraphics[2], 0, 0, 64, 64, this.gridMouse[0], this.gridMouse[1], 64, 64);
          // Draw selected object
          if (this.selectedObject != null) {
            this.ctx.globalAlpha = 0.5;
            this.drawTileFromSet(this.selectedObject.GraphicsID, this.gridMouse[0], this.gridMouse[1]);
            this.ctx.globalAlpha = 1.0;
          }
        }

        //Draw remote client if any on tile
        var mapKey = i + ":" + j;
        if (remotePlayersTileMap[mapKey] != undefined) {
          for (var k = 0; k < this.remotePlayers.length; k++) {
            this.drawRemotePlayer(remotePlayersTileMap[mapKey]);
          }
        }
        //Draw Player
        if (i == playerTile[0] && j == playerTile[1]) {
          this.drawLocalPlayer();
        }

        if (objectTileId >= 0)
          this.drawTileFromSet(objectTileId, worldX, worldY);

        //Draw objects on tile
        if (objectTile.Objects != null) {
          for (var k = 0; k < objectTile.Objects.length; k++) {
            this.drawTileFromSet(objectTile.Objects[k].GraphicsID, worldX, worldY);
          }
        }
      }
    }

    //this.drawRemotePlayers();
  }

  drawLocalPlayer() {
    var rotOffset = this.localPlayer.rotation * 64;
    this.ctx.fillText(this.localPlayer.name, this.localPlayer.x, this.localPlayer.y - 16);
    if (this.localPlayer.address == 'd3facf8ea92f69ff2f7e65bf0d0f556168ba24e4d88343e80c281d65accca406') {
      this.ctx.drawImage(this.tileGraphics[3], rotOffset, 0, 64, 96, this.localPlayer.x - 32, this.localPlayer.y - 64 - 32 * this.localPlayer.z, 64, 96);
    } else {
      this.ctx.drawImage(this.tileGraphics[1], 0, 0, 64, 64, this.localPlayer.x - 32, this.localPlayer.y - 32, 64, 64);
    }
  }

  drawRemotePlayer(i) {
    var rotOffset = this.remotePlayers[i].rotation * 64;
    if (this.remotePlayers[i].address == 'd3facf8ea92f69ff2f7e65bf0d0f556168ba24e4d88343e80c281d65accca406') {
      this.ctx.drawImage(this.tileGraphics[3], rotOffset, 0, 64, 96, this.remotePlayers[i].x - 32, this.remotePlayers[i].y - 64, 64, 96);
    } else {
      this.ctx.drawImage(this.tileGraphics[1], 0, 0, 64, 64, this.remotePlayers[i].x - 32, this.remotePlayers[i].y - 32, 64, 64);
    }
    this.ctx.fillText(this.remotePlayers[i].name, this.remotePlayers[i].x, this.remotePlayers[i].y - 16);
  }

  draw() {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.ctx.textAlign = "center";
    this.ctx.font = "16px Arial";
    this.ctx.fillStyle = "#fff";

    if (this.localPlayer != null) {

      //Move over pathfinding
      if (this.currentPath != null && this.currentPath.length > 0) {
        var pathTileX = this.currentPath[0].x;
        var pathTileY = this.currentPath[0].y;

        var pathToScreen = this.get2dFromTileCoordinates(pathTileX, pathTileY, 32);
        var path = this.cartesianToIso(pathToScreen[0], pathToScreen[1]);
        path[0] += this.camX + 32;
        path[1] += this.camY + 32;


        var speed = 1.0;
        var xDif = path[0] - this.localPlayer.x;
        var yDif = path[1] - this.localPlayer.y;

        if (Math.abs(xDif) > 0 && Math.abs(yDif) > 0) {
          var dif = Math.sqrt(xDif * xDif + yDif * yDif);
          if (dif > 1) {
            var dx = xDif / dif;
            var dy = yDif / dif;
            this.localPlayer.x += dx * speed;
            this.localPlayer.y += dy * speed;

            //Get rotation
            var angle = Math.atan2(dy, dx); //radians
            var degrees = 180 * angle / Math.PI; //degrees
            var degFinal = (360 + Math.round(degrees)) % 360;
            var degEights = Math.floor((degFinal + 22.5) / 45.0);
            this.localPlayer.rotation = degEights % 8;
            //this.localPlayer.rotation = ( / 45;
          } else {
            this.currentPath.shift();
            this.localPlayer.nx = path[0];
            this.localPlayer.ny = path[1];
            setTimeout(update, 0);
          }
        } else {
          this.currentPath.shift();
          this.localPlayer.nx = path[0];
          this.localPlayer.ny = path[1];
          setTimeout(update, 0);
        }
      }
      /*
      //If new pos move to slowly
      if (this.localPlayer.nx != null) {
        var speed = 1.0;
        var xDif = this.localPlayer.nx - this.localPlayer.x;
        var yDif = this.localPlayer.ny - this.localPlayer.y;

        if (Math.abs(xDif) > 0 && Math.abs(yDif) > 0) {
          var dif = Math.sqrt(xDif * xDif + yDif * yDif);
          if (dif > 1) {
            var dx = xDif / dif * speed;
            var dy = yDif / dif * speed;
            this.localPlayer.x += dx;
            this.localPlayer.y += dy;
          }
        }
      }*/
    }

    //If new pos move to slowly
    for (var i = 0; i < this.remotePlayers.length; i++) {
      if (new Date().getTime() - this.remotePlayers[i].lastUpdate > 60 * 1000) {
        this.remotePlayers.splice(i, 1);
        continue;
      }
      if (isNaN(this.remotePlayers[i].x)) this.remotePlayers[i].x = this.remotePlayers[i].nx;
      if (isNaN(this.remotePlayers[i].y)) this.remotePlayers[i].y = this.remotePlayers[i].ny;

      var speed = 1.0;
      var xDif = this.remotePlayers[i].nx - this.remotePlayers[i].x;
      var yDif = this.remotePlayers[i].ny - this.remotePlayers[i].y;

      if (Math.abs(xDif) > 0 && Math.abs(yDif) > 0) {
        var dif = Math.sqrt(xDif * xDif + yDif * yDif);
        if (dif > 1) {
          var dx = xDif / dif * speed;
          var dy = yDif / dif * speed;
          this.remotePlayers[i].x += dx;
          this.remotePlayers[i].y += dy;
        }
      }
    }

    if (new Date().getTime() - this.lastMessageTime > 10000) {
      this.addMessage('', '');
      if (this.chatMessages.length > 10) {
        this.chatMessages.pop();
      }
    }

    if (this.graphicsLoaded)
      this.drawMap();

    this.drawSpeechBubbles();

    easystar.calculate();
    //drawBall();

    for (var i = 0; i < this.currentPath.length; i++) {
      var pathToScreen = this.get2dFromTileCoordinates(this.currentPath[i].x, this.currentPath[i].y, 32);
      var path = this.cartesianToIso(pathToScreen[0], pathToScreen[1]);
      path[0] += this.camX + 32;
      path[1] += this.camY + 32;

      this.drawBall(path[0], path[1] + 16, 1.0, '#FFF');
    }
  }

  speechBubble(text, x, y, alpha) {
    var messure = this.ctx.measureText(text);

    var w = messure.width;
    var h = 20;

    this.ctx.beginPath();
    this.ctx.strokeStyle = "rgba(0, 0, 0, " + alpha + ")";
    this.ctx.lineWidth = "1";
    this.ctx.fillStyle = "rgba(255, 255, 255, " + alpha + ")";

    this.ctx.moveTo(x, y);
    //this.ctx.lineTo(x + (w * 0.2), y);
    //this.ctx.lineTo(x + (w * 0.2), y + 10);
    //this.ctx.lineTo(x + (w * 0.3), y);
    this.ctx.lineTo(x + w, y);
    this.ctx.quadraticCurveTo(x + (w * 1.1), y, x + (w * 1.1), y - (h * 0.2)); // corner: right-bottom
    this.ctx.lineTo(x + (w * 1.1), y - (h * 0.8)); // right
    this.ctx.quadraticCurveTo(x + (w * 1.1), y - h, x + w, y - h); // corner: right-top
    this.ctx.lineTo(x, y - h); // top
    this.ctx.quadraticCurveTo(x - (w * 0.1), y - h, x - (w * 0.1), y - (h * 0.8)); // corner: left-top
    this.ctx.lineTo(x - (w * 0.1), y - (h * 0.2)); // left
    this.ctx.quadraticCurveTo(x - (w * 0.1), y, x, y); // corner: left-bottom

    this.ctx.fill();
    this.ctx.stroke();
    this.ctx.closePath();

    this.ctx.textAlign = 'left';
    this.ctx.fillStyle = "rgba(0, 0, 0, " + alpha + ")";
    this.ctx.fillText(text, x, y - 6);
  }

  containsPlayer(x, y, player) {
    var dx = x - player.x;
    var dy = y - player.y;
    var d = Math.sqrt(dx * dx + dy * dy);
    if (d < 16)
      return true;
    //for (var i = 0; i < this.remotePlayers.length; i++) {
  }

  getPlayerByKey(publicKey) {
    if (publicKey == this.localPlayer.address)
      return this.localPlayer;
    return this.remotePlayers.filter((x) => {
      return x.address = publicKey;
    })[0];
  }

  updateObjectView(address) {
    var getClientURL = 'http://13.95.225.207/NKNHotelBackend/api/values/';
    $('#infoItems').html("Loading...");
    $.getJSON(getClientURL + address, function(data) {
      if (data == null) {
        $('#infoItems').html("None");
        return;
      }
      $('#infoItems').html("- " + data.objects);
    });
  }

  getRoom(address) {
    var getClientURL = 'http://13.95.225.207/NKNHotelBackend/api/room/get/';
    $.getJSON(getClientURL + address, function(data) {
      console.log(data);
      if (data != null) {
        this.objectMap = new Array(data.TileLayers[0].length).fill(0).map(() => new Array(data.TileLayers[0][0].length).fill(0));
      } else {
        this.objectMap = new Array(8).fill(0).map(() => new Array(10).fill(0));
        return;
      }
      for (var i = 0; i < data.TileLayers[0].length; i++) {
        for (var j = 0; j < data.TileLayers[0][0].length; j++) {
          this.objectMap[i][j] = data.TileLayers[0][i][j];
        }
      }
      this.currentRoom = address;
    }.bind(this));
  }

  postMoveObject(address, objectId, objectX, objectY) {
    var getClientURL = 'http://13.95.225.207/NKNHotelBackend/api/room/moveobject/';
    $.ajax({
      url: getClientURL + address,
      type: 'POST',
      data: JSON.stringify({
        "objectId": objectId,
        "objectX": objectX,
        "objectY": objectY
      }),
      dataType: "json",
      contentType: "application/json; charset=utf-8",
      success: function(data) {
        this.getRoom(address);
        this.selectedObject = null;
      }.bind(this),
      failure: function(errMsg) {
        alert(errMsg);
      }
    });
  }

  isoToScreen(x, y) {
    var tileW = 32;
    var tileH = 32;
    var posX = (x - y) * tileW;
    var posY = (x + y) * tileH / 2;

    return [posX, posY];
  };

  screenToIso1(x, y) {
    var tileW = 32;
    var tileH = 32;
    var posX = ((y * 2 / tileH) + (x / tileW)) / 2;
    var posY = (y * 2 / tileH) - posX;

    return [Math.floor(posX + 0.5), Math.floor(posY + 0.5)];
  }

  screenToIso2(x, y) {
    var tileW = 64;
    var tileH = 32;
    var mouse_grid_x = Math.floor((y / tileH) + (x / tileW));
    var mouse_grid_y = Math.floor((-x / tileW) + (y / tileH));

    return [mouse_grid_x, mouse_grid_y];
  }

  /**
   * convert an isometric point to 2D
   * */
  isoToCartesian(x, y) {
    //gx=(2*isoy+isox)/2;
    //gy=(2*isoy-isox)/2
    var rX = (2 * y + x) / 2;
    var rY = (2 * y - x) / 2;
    return [rX, rY];
  }
  /**
   * convert a 2d point to isometric
   * */
  cartesianToIso(x, y) {
    //gx=(isox-isoxy;
    //gy=(isoy+isox)/2
    var rX = x - y;
    var rY = (x + y) / 2;
    return [rX, rY];
  }

  /**
   * convert a 2d point to specific tile row/column
   * */
  getTileCoordinates(x, y, tileHeight) {
    var rX = Math.floor(x / tileHeight);
    var rY = Math.floor(y / tileHeight);

    return [rX, rY];
  }

  /**
   * convert specific tile row/column to 2d point
   * */
  get2dFromTileCoordinates(x, y, tileHeight) {
    var rX = x * tileHeight;
    var rY = y * tileHeight;

    return [rX, rY];
  }



  runGame() {
    var x = 384;
    var y = 230;
    var color = "#" + ((1 << 24) * Math.random() | 0).toString(16);

    this.localPlayer = {
      "address": this.clientAddress,
      "x": x,
      "y": y,
      "z": 0,
      "nx": x,
      "ny": y,
      "name": name,
      "rotation": 0
    }
    document.addEventListener("keydown", this.keyDownHandler.bind(this), false);
    document.addEventListener("keyup", this.keyUpHandler, false);

    this.canvas.addEventListener('mousemove', (e) => {
      var mouseX = e.pageX - this.canvas.offsetLeft - 16;
      var mouseY = e.pageY - this.canvas.offsetTop - 16;
      /*console.log([mouseX, mouseX]);

      var isoPos = this.cartesianToIso(mouseX, mouseY);
      console.log(isoPos);

      var resultPos = this.isoToScreen(isoPos[0], isoPos[1]);
      console.log(resultPos);*/
      var isoCoord = this.isoToCartesian(mouseX - this.camX - 16, mouseY - this.camY - 16);
      this.tileMouse = this.getTileCoordinates(isoCoord[0], isoCoord[1], 32);
      var tileToScreen = this.get2dFromTileCoordinates(this.tileMouse[0], this.tileMouse[1], 32);
      this.gridMouse = this.cartesianToIso(tileToScreen[0], tileToScreen[1]);
      this.gridMouse[0] += this.camX;
      this.gridMouse[1] += this.camY;
    });

    this.canvas.addEventListener('click', (e) => {

      //If we are moving an object place first
      if (this.selectedObject != null) {
        this.postMoveObject(this.localPlayer.address, this.selectedObject.ID, this.tileMouse[0], this.tileMouse[1]);
      }

      var mouseX = e.pageX - this.canvas.offsetLeft;
      var mouseY = e.pageY - this.canvas.offsetTop;

      var localClicked = (this.containsPlayer(mouseX, mouseY, this.localPlayer));
      if (localClicked) {
        $('#infoName').html(this.localPlayer.name);
        $('#infoAddress').html(this.localPlayer.address);
        $('#infoBalance').html('Loading...');
        this.updateObjectView(this.localPlayer.address);

        wallet.http.getBalanceByAddr(wallet.address).then((result) => {
          $('#infoBalance').html('' + result.amount);
        })
        $('button').prop('disabled', false);
      }

      var anyRemote = false;
      for (var i = 0; i < this.remotePlayers.length; i++) {
        var remoteClicked = (this.containsPlayer(e.clientX - this.canvas.offsetLeft, e.clientY - this.canvas.offsetTop, this.remotePlayers[i]));
        if (remoteClicked) {
          anyRemote = true;
          $('#infoName').html(this.remotePlayers[i].name);
          $('#infoAddress').html(this.remotePlayers[i].address);
          this.updateObjectView(this.remotePlayers[i].address);

          //Get wallet address
          var signatureRedeem = wallet.protocol.publicKeyToSignatureRedeem(this.remotePlayers[i].address);
          var programHash = wallet.protocol.hexStringToProgramHash(signatureRedeem);
          var walletAddr = wallet.protocol.programHashStringToAddress(programHash);

          $('#infoBalance').html('Loading...');
          wallet.http.getBalanceByAddr(walletAddr).then((result) => {
            $('#infoBalance').html('' + result.amount);
          })
          $('button').prop('disabled', false);
        }
      }

      var isObjectClicked = false;
      if (this.objectMap[this.tileMouse[0]][this.tileMouse[1]].Objects != undefined) {
        if (this.objectMap[this.tileMouse[0]][this.tileMouse[1]].Objects[0] != undefined) {
          this.selectedObject = this.objectMap[this.tileMouse[0]][this.tileMouse[1]].Objects[0];
          console.log(this.selectedObject);
          isObjectClicked = true;
          var objectInfo = ObjectInfo['' + this.selectedObject.GraphicsID];
          $('#itemInfo').html(objectInfo.name + '<br>' + GetRarity(this.selectedObject.ID));

          if (this.localPlayer.address != this.currentRoom) {
            this.selectedObject = null;
          }
        }
      }

      if (!localClicked && !remoteClicked && !isObjectClicked) {
        if (this.floorMap[this.tileMouse[0]] == undefined || this.floorMap[this.tileMouse[0]][this.tileMouse[1]] == undefined)
          return;

        var playerIso = this.isoToCartesian(this.localPlayer.x - this.camX - 16, this.localPlayer.y - this.camY - 16);
        var playerTile = this.getTileCoordinates(playerIso[0], playerIso[1], 32);

        //pathfinding
        easystar.findPath(playerTile[0], playerTile[1], this.tileMouse[0], this.tileMouse[1], function(path) {
          if (path === null) {
            return;
          } else {
            this.currentPath = path;
            if (this.currentPath.length > 0) {
              var pathTileX = this.currentPath[0].x;
              var pathTileY = this.currentPath[0].y;

              var pathToScreen = this.get2dFromTileCoordinates(pathTileX, pathTileY, 32);
              var path = this.cartesianToIso(pathToScreen[0], pathToScreen[1]);
              path[0] += this.camX + 32;
              path[1] += this.camY + 32;
              this.localPlayer.nx = path[0];
              this.localPlayer.ny = path[1];
              setTimeout(update, 0);
            }
          }
        }.bind(this));

        /*if (this.floorMap[this.tileMouse[0]][this.tileMouse[1]] == 1) {
          //$('#infoName').html('none');
          //$('#infoAddress').html('none');
          this.localPlayer.nx = this.gridMouse[0] + 32;
          this.localPlayer.ny = this.gridMouse[1] + 32;
          setTimeout(update, 0);
        }*/
      }
    });
    setInterval(this.draw.bind(this), 16);
    //setInterval(this.updateClients.bind(this), 5000);
  }
}